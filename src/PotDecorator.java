import java.util.Objects;

public class PotDecorator implements Decoration {

  private int id;
  private double price;
  private ShopItem item;
  private String color;
  private String material;
  private double height;
  private double width;
  private double volume;

  public PotDecorator(ShopItem item) {
    this.item = item;
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public double getPrice() {
    return this.price + this.item.getPrice();
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getVolume() {
    return volume;
  }

  public void setVolume(double volume) {
    this.volume = volume;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PotDecorator potDecorator = (PotDecorator) o;
    return id == potDecorator.id &&
        Double.compare(potDecorator.price, price) == 0 &&
        Double.compare(potDecorator.height, height) == 0 &&
        Double.compare(potDecorator.width, width) == 0 &&
        Double.compare(potDecorator.volume, volume) == 0 &&
        Objects.equals(color, potDecorator.color) &&
        Objects.equals(material, potDecorator.material);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, color, material, height, width, volume);
  }

  @Override
  public String toString() {
    return "Flower{" +
        "ID = " + id + '\'' +
        ", price =" + price +
        ", height =" + height +
        ", color ='" + color + '\'' +
        ", width =" + width +
        ", volume =" + volume +
        '}';
  }
}

