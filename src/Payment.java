public interface Payment {

  void pay(double price);
}
