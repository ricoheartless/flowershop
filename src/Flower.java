import java.util.Date;
import java.util.Objects;

public class Flower implements ShopItem {

  public Flower(String name, double price) {
    this.name = name;
    this.price = price;
  }

  private int id;
  private String name;
  private double price;
  private double height;
  private String color;
  private boolean spikes;
  private Date date;

  @Override
  public String toString() {
    return "Flower{" +
        "ID = " + id + '\'' +
        "name ='" + name + '\'' +
        ", price =" + price +
        ", height =" + height +
        ", color ='" + color + '\'' +
        ", spikes =" + spikes +
        ", date =" + date +
        '}';
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Flower flower = (Flower) o;
    return Double.compare(flower.price, price) == 0 &&
        Objects.equals(name, flower.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, price);
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public boolean isSpikes() {
    return spikes;
  }

  public void setSpikes(boolean spikes) {
    this.spikes = spikes;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}
