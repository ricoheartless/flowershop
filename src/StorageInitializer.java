import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StorageInitializer {

  public static Map<ShopItem, Integer> initReservedShopItems() {
    Map<ShopItem, Integer> map = new HashMap<>();
    ShopItem i1 = new Flower("Rose", 25.5);
    ((Flower) i1).setColor("Red");
    ((Flower) i1).setHeight(19);
    ((Flower) i1).setSpikes(true);
    ((Flower) i1).setDate(new Date(2018, 11, 13));


    ShopItem i2 = new Flower("Anemone", 18);
    ((Flower) i2).setColor("White");
    ((Flower) i2).setHeight(12);
    ((Flower) i2).setSpikes(false);
    ((Flower) i2).setDate(new Date(2018, 11, 12));

    ShopItem i3 = new Flower("Daisy", 27);
    ((Flower) i3).setColor("Pink");
    ((Flower) i3).setHeight(14);
    ((Flower) i3).setSpikes(false);
    ((Flower) i3).setDate(new Date(2018, 11, 12));


    ShopItem i4 = new Flower("Petite Rose", 35);
    ((Flower) i4).setColor("Yellow");
    ((Flower) i4).setHeight(18);
    ((Flower) i4).setSpikes(false);
    ((Flower) i4).setDate(new Date(2018, 11, 13));


    ShopItem i5 = new Cactus();
    ((Cactus) i5).setBloom(true);
    ((Cactus) i5).setForm("Tall");
    ((Cactus) i5).setSpikeLength(2);
    ((Cactus) i5).setPrice(120);

    Decoration pot = new PotDecorator();
    ((PotDecorator) pot).setColor("brown");
    ((PotDecorator) pot).setPrice(20);
    ((PotDecorator) pot).setMaterial("clay");
    ((Cactus) i5).addDecoration(pot);

    map.put(i5, 5);


    ShopItem b1 = new Bouqete();
    //Bouquet with 3 + 2 flowers and one pot decoration
    ((Bouqete) b1).addFlower((Flower) i3, 3);
    ((Bouqete) b1).addFlower((Flower) i1, 2);
    ((Bouqete) b1).setId(11);

    Decoration string = new DecorationString();
    ((DecorationString) string).setColor("Red");
    ((DecorationString) string).setId(12);
    ((DecorationString) string).setLength(10);
    ((DecorationString) string).setPrice(10);
    ((Bouqete) b1).addDecoration(string);

    map.put(b1, 3);

    return map;
  }

  public static Map<ShopItem, Integer> initShopItems(){

    Map<ShopItem, Integer> map = new HashMap<>();
    ShopItem i1 = new Flower("Rose", 26);
    ((Flower) i1).setColor("Red");
    ((Flower) i1).setHeight(19);
    ((Flower) i1).setSpikes(true);
    ((Flower) i1).setDate(new Date(2018, 11, 13));
    ((Flower) i1).setId(5);
    map.put(i1, 40);

    ShopItem i2 = new Flower("Anemone", 18);
    ((Flower) i2).setColor("White");
    ((Flower) i2).setHeight(12);
    ((Flower) i2).setSpikes(false);
    ((Flower) i2).setDate(new Date(2018, 11, 12));
    ((Flower) i2).setId(6);
    map.put(i2, 55);

    ShopItem i3 = new Flower("Daisy", 27);
    ((Flower) i3).setColor("Pink");
    ((Flower) i3).setHeight(14);
    ((Flower) i3).setSpikes(false);
    ((Flower) i3).setDate(new Date(2018, 11, 12));
    ((Flower) i3).setId(7);
    map.put(i3, 52);

    ShopItem i4 = new Flower("Petite Rose", 35);
    ((Flower) i4).setColor("Yellow");
    ((Flower) i4).setHeight(18);
    ((Flower) i4).setSpikes(false);
    ((Flower) i4).setDate(new Date(2018, 11, 13));
    ((Flower) i4).setId(8);
    map.put(i4, 54);

    ShopItem i5 = new Flower("RoseWhite", 28);
    ((Flower) i5).setColor("White");
    ((Flower) i5).setHeight(20);
    ((Flower) i5).setSpikes(true);
    ((Flower) i5).setDate(new Date(2018, 11, 13));
    ((Flower) i5).setId(9);
    map.put(i5, 43);

    return map;

  }


  public static Map<Decoration, Integer> initDecorations(){
    Map<Decoration, Integer> decorations = new HashMap<>();
    Decoration d1 = new PotDecorator();
    ((PotDecorator) d1).setColor("White");
    ((PotDecorator) d1).setHeight(20);
    ((PotDecorator) d1).setId(10);
    ((PotDecorator) d1).setMaterial("clay");
    ((PotDecorator) d1).setPrice(47);
    ((PotDecorator) d1).setWidth(18);
    ((PotDecorator) d1).setVolume(2000);
    decorations.put(d1, 20);

    Decoration d2 = new StickDecorator();
    ((StickDecorator) d2).setPrice(30);
    ((StickDecorator) d2).setMaterial("wood");
    ((StickDecorator) d2).setId(11);
    ((StickDecorator) d2).setColor("wood");
    ((StickDecorator) d2).setLength(30);
    decorations.put(d2, 20);

    Decoration d3 = new DecorationString();
    ((DecorationString) d3).setPrice(5);
    ((DecorationString) d3).setLength(50);
    ((DecorationString) d3).setId(12);
    ((DecorationString) d3).setColor("blue");
    decorations.put(d3, 1);

    return decorations;

  }



}
