import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Bouqete implements ShopItem {

  private int id;
  private double price;
  private Map<Flower, Integer> flowerIntegerMap = new HashMap<>();

  public Bouqete() {
  }

  public void addFlower(Flower flower, Integer num) {
    flowerIntegerMap.put(flower, num);
    this.price += flower.getPrice();
  }

  public void removeFlower(Flower flower, Integer num) {
    if (flowerIntegerMap.get(flower) < num) {
      for (Flower f : flowerIntegerMap.keySet()) {
        this.price -= f.getPrice();
      }
      flowerIntegerMap.remove(flower);
    } else {
      this.price -= flower.getPrice() * num;
      int newNum = flowerIntegerMap.get(flower) - num;
      flowerIntegerMap.replace(flower, newNum);
    }
  }

  public Map<Flower, Integer> getFlowerIntegerMap() {
    return flowerIntegerMap;
  }

  public void setFlowerIntegerMap(Map<Flower, Integer> flowerIntegerMap) {
    this.flowerIntegerMap = flowerIntegerMap;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Bouqete bouqete = (Bouqete) o;
    return id == bouqete.id &&
        Double.compare(bouqete.price, price) == 0 &&
        Objects.equals(flowerIntegerMap, bouqete.flowerIntegerMap);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, flowerIntegerMap);
  }

  @Override
  public int getId() {
    return this.id;
  }


  public void setId(int id) {
    this.id = id;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  @Override
  public double getPrice() {
    return this.price;
  }


  @Override
  public String toString() {
    return "Bouqete{" +
        "ID= " + id + '\'' +
        "name= 'bouqete" + '\'' +
        ", price=" + price +
        '}';
  }

}