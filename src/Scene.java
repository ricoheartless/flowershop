
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Flow;


public class Scene {

  Shop shop;

  public static void startScene(Shop shop) {
    this.shop = shop;

  }

  private static void createBougete() {
    Bouqete bouqete = new Bouqete();
  }

  private static void addReservedStore() {
    reservedStore = new Store();
    reservedStore.setFlowerStore(StorageInitializer.initReservedShopItems());
  }

  private static void addStore() {
    store = new Store();
    store.setFlowerStore(StorageInitializer.initShopItems());
  }

  private static void firstActivity() {

    do {
      System.out.println("Press 1 to watch list of store items");
      System.out.println("Press 2 to to create your own buqet");
      System.out.println("Press 0 to quit");
      int i;
      Scanner scanner = new Scanner(System.in);
      try {
        i = scanner.nextInt();
      } catch (Exception e) {
        i = 0;
      }

      if (i == 1) {
        storeItemsActivity();
      } else if (i == 2) {
        flowersActivity();
      } else if (i == 0) {
        return;
      } else {
        System.out.println("Wrong input, try again");
      }

    } while (true);

  }


  private static void showOrder() {
    String line = "+-----------------+";
    System.out.println("Your order: ");
    if (order.getOrderList().isEmpty()) {
      System.out.println("  empty");
    } else {
      for (ShopItem item : order.getOrderList()) {
        System.out.println("  " + item);
//        total = total + item.getPrice();
      }
      System.out.println(line);
      System.out.print("| Total: " + total);

      for (int i = 0; i < (line.length() - (3 + String.valueOf(total).length())); i++) {
        System.out.print(" ");
      }
      System.out.println("|");
      System.out.println(line);

    }
  }

  private static void storeItemsActivity() {

    ///Output of all Items
    System.out.println("Items: ");
    for (Map.Entry<ShopItem, Integer> entry : reservedStore.getFlowerStore().entrySet()) {
      System.out.println("  " + entry.getKey());
      System.out.println("  Left in store: (" + entry.getValue() + ")");
    }

    showOrder();

    do {

      System.out.println();
      System.out.println("Enter ID of item to add it in cart");
      System.out.println("Press 1 to go in previous menu");
      System.out.println("Press 0 to quit");

      Scanner scanner = new Scanner(System.in);
      int i = scanner.nextInt();
      if (i == 1) {
        firstActivity();
      } else if (i == 0) {
        System.exit(0);
      } else if (i > 1) {
        //Add item to cart
        addItemToOrder(i);
      } else {
        System.out.println("Wrong input, try again");
      }

    } while (true);
  }


  private static void flowersActivity() {

    //All flowers list
    System.out.println("Flowers in store: ");
    for (Map.Entry<ShopItem, Integer> entry : store.getFlowerStore().entrySet()) {
      System.out.println("  " + entry.getKey());
      System.out.println("  Number left: (" + entry.getValue() + ")");
    }

    System.out.println("Selected flowers: ");

    if (bouqete.getFlowerIntegerMap().size() == 0) {
      System.out.println("  now is empty");
    } else {
      for (Map.Entry<Flower, Integer> entry : bouqete.getFlowerIntegerMap().entrySet()) {
        System.out.println("  " + entry.getKey());
        System.out.println("  number of flowers: " + entry.getValue());
      }
      System.out.println("Decorations: ");
      if(bouqete.getDecorations() != null) {
        for (Decoration d : bouqete.getDecorations()) {
          System.out.println("  " + d);
        }
      }
    }

    showOrder();

    do {

      System.out.println();
      System.out.println("Enter ID of flower to add it to bouqete");
      if(readyForBouqete){
        System.out.println("Press 2 to add decoration");
      }
      System.out.println("Press 1 to add your bouqete to order");
      System.out.println("Press 0 to quit");

      Scanner scanner = new Scanner(System.in);
      int i = scanner.nextInt();


      if (i == 2 ) {
        addDecorationsActivity();
      } else if (i == 0) {
        return;
      } else if (i > 2) {
        //add flower in buqet
        addFlowerToBouqete(i);
      } else if (i == 1) {
        addBouqeteToOrder();
      } else {
        System.out.println("Wrong input,try again");
      }
    } while (true);
  }

  private static void addBouqeteToOrder() {
    bouqete.setId(id++);
    order.addItem(bouqete);
    bouqete = new Bouqete();
    flowersActivity();

  }

  private static void addDecorationsActivity() {
    //All decorations list
    System.out.println("Available decorations: ");
    for (Map.Entry<Decoration, Integer> entry : store.getDecorationsStorage().entrySet()) {
      System.out.print(entry.getKey());
      System.out.println(" Left in store: (" + entry.getValue() + ")");
    }

    do {
      System.out.println();
      System.out.println("Enter ID of decoration to add it to boqete");
      System.out.println("Press 1 to add bouqete to order");
      System.out.println("Press 0 to quit");

      Scanner scanner = new Scanner(System.in);
      int i = scanner.nextInt();
      if (i == 1) {
        //continue
        addBouqeteToOrder();

      } else if (i == 0) {
        return;
      } else if (i > 1) {
        //add decoration in buqet
        addDecoration(i);
      } else {
        System.out.println("Wrong input,try again");
      }
    } while (true);

  }

  private static void addDecoration(int id) {
    if (store.findDecorationById(id) != null) {
      bouqete.addDecoration(store.findDecorationById(id));
      store.removeDecoration(store.findDecorationById(id));
    } else {
      addDecorationsActivity();
    }

    flowersActivity();

  }

  private static void addFlowerToBouqete(int id) {
    try {
      ShopItem item = store.findShopItemById(id);
      if (item == null) {
        flowersActivity();
      } else {
        if (bouqete.getFlowerIntegerMap().get(item) != null) {
          bouqete.addFlower((Flower) item, bouqete.getFlowerIntegerMap().get(item) + 1);
          total = total + item.getPrice();
          store.removeShopItem(item);
        } else {
          bouqete.addFlower((Flower) item, 1);
          total = total + item.getPrice();
          store.removeShopItem(item);
        }
      }
      if ((bouqete.getFlowerIntegerMap().get(item) > 1)||(bouqete.getFlowerIntegerMap().size() > 1)) {
        readyForBouqete = true;
      }

      flowersActivity();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private static void addItemToOrder(int id) {

    ShopItem item = reservedStore.findShopItemById(id);
    if (item == null) {
      storeItemsActivity();
    } else {
      order.addItem(item);
      total = total + item.getPrice();
      reservedStore.removeShopItem(item);
    }
    storeItemsActivity();


  }
}