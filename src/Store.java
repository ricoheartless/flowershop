import java.util.HashMap;
import java.util.Map;

public class Store {

  //private Map<Decoration, Integer> decorationsStorage = new HashMap<>();
  private Map<ShopItem, Integer> flowerStore = new HashMap<>();

  public void addShopItem(ShopItem item, Integer quantity) {
    flowerStore.put(item, quantity);
  }

  public void removeShopItem(ShopItem item) {
    int quantity = flowerStore.get(item);
    if (quantity > 1) {
      flowerStore.replace(item, quantity - 1);
    } else {
      flowerStore.remove(item);
    }

  }



  public Map<ShopItem, Integer> getFlowerStore() {
    return flowerStore;
  }

  public ShopItem findShopItemById(int id) {
    ShopItem shopItem = null;
    for (ShopItem item : flowerStore.keySet()) {
      if (item.getId() == id) {
        shopItem = item;
      }
    }
    return shopItem;
  }


  public void setFlowerStore(Map<ShopItem, Integer> flowerStore) {
    this.flowerStore = flowerStore;
  }
}
