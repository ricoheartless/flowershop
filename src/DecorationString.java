import java.util.Objects;

public class DecorationString implements Decoration {

  private ShopItem item;
  private int id;
  private double price;
  private String color;
  private double length;

  public DecorationString(ShopItem item) {
    this.item = item;
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  @Override
  public double getPrice() {
    //Default price for 1 unit of measurement
    return this.getCalculatedPrice() + this.item.getPrice();
  }

  public double getCalculatedPrice() {
    //To calculate price for certain length
    return this.price * this.length;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DecorationString that = (DecorationString) o;
    return id == that.id &&
        Double.compare(that.price, price) == 0 &&
        Double.compare(that.length, length) == 0 &&
        Objects.equals(color, that.color);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, color, length);
  }
  @Override
  public String toString() {
    return "Flower{" +
        "ID = " + id + '\'' +
        ", price =" + price +
        ", color ='" + color + '\'' +
        "length ='" + length + '\'' +
        '}';
  }
}
