import java.util.List;

public interface Delivery {
  void deliver(List<ShopItem> item);
}
