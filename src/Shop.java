import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Shop {

  Store store;
  Payment payment;
  Delivery delivery;
  List<ShopItem> orderList;


  public double calculateTotalPrice() {
    double total = 0;
    for (ShopItem item : orderList) {
      total += item.getPrice();
    }
    return total;
  }

  public void processOrder() {
    System.out.println("Your order: ");
    orderList.toString();
    System.out.println("Is proceeded");
  }

  public Payment getPayment() {
    return payment;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }

  public Delivery getDelivery() {
    return delivery;
  }

  public void setDelivery(Delivery delivery) {
    this.delivery = delivery;
  }

  public List<ShopItem> getOrderList() {
    return orderList;
  }

  public void setOrderList(List<ShopItem> orderList) {
    this.orderList = orderList;
  }


  public void addItem(ShopItem item) {
    orderList.add(item);
  }

  public void addByIdItem(int id){
    ShopItem item = store.findShopItemById(id);
    orderList.add(item);
  }

  public void removeItem(ShopItem item) {
    orderList.remove(item);
  }

  public Shop() {
   orderList = new LinkedList<>();
  }

  public void showFlowerStore() {
    System.out.println(store.getFlowerStore().toString());
  }



  public void setStore(Store store) {
    this.store = store;
  }

  public void userSelectionProcess() {
    Scanner scanner = new Scanner(System.in);
    int input;
    do {
      while (!scanner.hasNextInt()) {
        scanner.next();
      }
      input = scanner.nextInt();
      switch (input) {
        case 0:
          continue;
        case 1:
          showFlowerStore();
          break;
        case 2:
          //orderList.add();
          createCustomBuquet();
          break;
        case 3:
          selectExsistingItem();
          break;
        default:
          System.out.println("Please enter correct num! Enter 2 to see list");
          break;
      }
    }
    while (input != 0);
  }

  public void createCustomBuquet(){ // void or not void???????

  }

  public void selectExsistingItem() { //?????

  }

}
