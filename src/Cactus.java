import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Cactus implements ShopItem {

  public Cactus(){}

  private int id;
  private double price;
  private String form;
  private double spikeLength;
  private boolean bloom;


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Cactus cactus = (Cactus) o;
    return id == cactus.id &&
        Double.compare(cactus.price, price) == 0 &&
        Double.compare(cactus.spikeLength, spikeLength) == 0 &&
        bloom == cactus.bloom &&
        Objects.equals(form, cactus.form);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, form, spikeLength, bloom);
  }

  @Override
  public int getId() {
    return this.id;
  }

  @Override
  public double getPrice() {
    return this.price;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getForm() {
    return form;
  }

  public void setForm(String form) {
    this.form = form;
  }

  public double getSpikeLength() {
    return spikeLength;
  }

  public void setSpikeLength(double spikeLength) {
    this.spikeLength = spikeLength;
  }

  public boolean isBloom() {
    return bloom;
  }

  public void setBloom(boolean bloom) {
    this.bloom = bloom;
  }


  @Override
  public String toString() {
    return "Cactus{" +
        "ID = " + id + '\'' +
        "spikeLength = " + spikeLength + '\'' +
        "form = " + form + '\'' +
        "is blooming = " + bloom + '\'' +
        ", price =" + price +
        '}';
  }
}
