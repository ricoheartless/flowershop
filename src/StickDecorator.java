import java.util.Objects;

public class StickDecorator implements Decoration {
  private int id;
  private double price;
  private ShopItem item;
  private double length;
  private String material;
  private String color;

  public StickDecorator() {
  }

  @Override
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public double getPrice() {
    return this.price + item.getPrice();
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StickDecorator stickDecorator = (StickDecorator) o;
    return id == stickDecorator.id &&
        Double.compare(stickDecorator.price, price) == 0 &&
        Double.compare(stickDecorator.length, length) == 0 &&
        Objects.equals(material, stickDecorator.material) &&
        Objects.equals(color, stickDecorator.color);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, price, length, material, color);
  }

  @Override
  public String toString() {
    return "Flower{" +
        "ID = " + id + '\'' +
        ", price =" + price +
        ", length =" + length +
        ", color ='" + color + '\'' +
        ", material =" + material +
        '}';
  }
}
