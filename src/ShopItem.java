public interface ShopItem {

  int getId();

  double getPrice();
}
